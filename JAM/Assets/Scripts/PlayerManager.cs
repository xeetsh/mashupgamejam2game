﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour {
    public GameObject m_spawnPoint;
    public UIController m_ui;

    private PlayerInputManager m_pim;

    private static PlayerManager instance;

    private void Awake() {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    private void Start() {
        m_pim = GetComponent<PlayerInputManager>();
    }
    private void OnPlayerJoined(PlayerInput input) {
        if (m_pim.playerCount > m_pim.maxPlayerCount) {
            Destroy(input.gameObject);
        } else { 
            // Set player position
            input.gameObject.transform.position = m_spawnPoint.transform.position;
            input.gameObject.transform.rotation = m_spawnPoint.transform.rotation;
            m_pim.DisableJoining();

            GetComponent<AudioSource>().Play();
            m_ui.PlayerJoin();
        }
    }
}
