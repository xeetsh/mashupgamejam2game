﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveController : MonoBehaviour {
    public float m_strength;
    public float m_radius;
    public float m_timeout;
    public float m_carStrengthModifier;
    public Renderer m_renderer;

    public List<string> m_triggers = new List<string>();
    public List<string> m_affects = new List<string>();

    private Rigidbody m_rb;
    private ParticleSystem m_ps;

    private float m_nextExplosion = 0f;
    private bool m_exploding = false;

    private void Start() {
        m_rb = GetComponent<Rigidbody>();
        m_ps = GetComponent<ParticleSystem>();
    }

    private void OnCollisionEnter(Collision collision) {
        if (m_triggers.Contains(collision.gameObject.tag)) {
            Explode();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (m_triggers.Contains(other.gameObject.tag)) {
            Explode(true);
        }
    }

    private void Explode(bool fake = false) {
        if (Time.time > m_nextExplosion || m_timeout == -1f) {
            m_exploding = true;

            // Get all affected objects in range
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, m_radius);

            foreach (Collider c in hitColliders) {
                // Check if object is affected by explosion
                if (m_affects.Contains(c.gameObject.tag)) {
                    Rigidbody rb = c.GetComponent<Rigidbody>();
                    float strength = m_strength;
                    if (c.CompareTag("Car")) {
                        strength *= m_carStrengthModifier;
                    }

                    if (rb != null) {
                        rb.AddExplosionForce(strength, transform.position, m_radius, 0.005f);
                    }

                // Ignite other explosives in range
                } else if (c.gameObject != gameObject && c.gameObject.CompareTag("Explosive")) {
                    ExplosiveController ec = c.GetComponent<ExplosiveController>();
                    if (ec != null && !ec.m_exploding) {
                        // Get Distance
                        float distance = (c.transform.position - transform.position).magnitude;
                        StartCoroutine(StartExplosionDelayed(ec, distance, fake));
                    }
                }
            }

            // If explosive is single use destroy it
            if (m_timeout == -1f) {
                StartCoroutine(ResolveExplosion());
            } else {
                m_nextExplosion = Time.time + m_timeout;
                m_exploding = false;
            }
        }
    }

    private IEnumerator StartExplosionDelayed(ExplosiveController ec, float distance, bool fake) {
        yield return new WaitForSeconds(distance / 30f);
        ec.Explode(fake);
        yield return null;
    }

    private IEnumerator ResolveExplosion() {
        m_ps.Play();
        m_renderer.enabled = false;
        m_rb.isKinematic = true;
        GetComponent<Collider>().enabled = false;

        yield return new WaitWhile(() => m_ps.isPlaying);

        //Destroy(gameObject);
        yield return null;
    }
}
