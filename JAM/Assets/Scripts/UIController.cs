﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public Text m_time;
    public Text m_item;
    public Text m_best;
    public GameObject m_actionCam;
    public GameObject m_startText;
    public GameObject m_controls;

    private PlayerController m_player;

    public void FirstStart() {
        m_item.enabled = false;
        m_controls.SetActive(false);
        m_time.enabled = false;
        m_best.enabled = false;

        m_startText.SetActive(true);
    }

    public void PlayerJoin() {
        m_item.enabled = true;
        m_controls.SetActive(true);
        m_time.enabled = true;
        m_best.enabled = true;

        m_startText.SetActive(false);
    }

    private void Update() {
        // Show current item
        if (m_player == null) {
            GameObject p = GameObject.FindGameObjectWithTag("Player");
            if (p != null) {
                m_player = p.GetComponent<PlayerController>();
            }
            m_item.text = "";
        } else {
            if (m_player.m_caryingIC != null && m_player.m_carying) {
                if (m_player.m_currentItem != 0)
                    m_item.text = m_player.m_caryingIC.m_name + " (" + m_player.m_itemAmmount[m_player.m_currentItem - 1].ToString() + "x)";
                else
                    m_item.text = "";
            } else {
                m_item.text = "";
            }
        }

        if (GameManager.manager != null && GameManager.manager.m_timerRunning) {
            m_actionCam.SetActive(true);
            m_time.text = GameManager.manager.m_time.ToString("F3").Replace(".", ":");
        } else if (GameManager.manager != null && !GameManager.manager.m_timerRunning && !GameManager.manager.m_goalReached) {
            m_time.text = "0:000";
            m_actionCam.SetActive(false);
        } else if (GameManager.manager != null && !GameManager.manager.m_timerRunning && GameManager.manager.m_goalReached) {
            m_time.text = GameManager.manager.m_time.ToString("F3").Replace(".", ":");
            m_actionCam.SetActive(true);
        }

        if (GameManager.manager.m_time < GameManager.best && GameManager.manager.m_goalReached) {
            m_best.text = "NEW BEST!";
        } else if (GameManager.best < Mathf.Infinity) {
            m_best.text = GameManager.best.ToString("F3").Replace(".", ":");
        } else {
            m_best.text = "";
        }
    }
}
