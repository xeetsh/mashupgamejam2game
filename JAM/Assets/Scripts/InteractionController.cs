﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class InteractionController : MonoBehaviour {
    public Material m_highlightMaterial;
    public Material m_ghostMaterial;
    public float m_caryingHeight;
    public string m_name;

    private Renderer m_renderer;
    private Material m_startMaterial;

    [HideInInspector] public Rigidbody m_rb;
    [HideInInspector] public MovableController m_mc;
    [HideInInspector] public Collider m_coll;

    [HideInInspector] public bool m_ready = false;




    private void Start() {
        m_renderer = GetComponentsInChildren<Renderer>().First(x => x.GetType() == typeof(MeshRenderer));
        m_startMaterial = m_renderer.material;
        m_rb = GetComponent<Rigidbody>();
        m_mc = GetComponent<MovableController>();
        m_coll = GetComponentInChildren<Collider>();

        m_ready = true;
    }

    public void Highlight(bool undo = false) {
        if (m_renderer != null && m_highlightMaterial != null && m_startMaterial != null) {
            if (!undo) {
                m_renderer.material = m_highlightMaterial;
            } else {
                m_renderer.material = m_startMaterial;
            }
        }
        
    }

    public void Ghost(bool undo = false) {
        if (!undo) {
            m_renderer.material = m_ghostMaterial;
        } else {
            m_renderer.material = m_startMaterial;
        }
    }
}
