﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    public float m_speed;
    public float m_rotSpeed;
    public float m_jumpHeigth;
    public GameObject m_centerOfMass;
    public GameObject m_cameraPostion;
    public float m_cameraMaxRot;

    public List<GameObject> m_items = new List<GameObject>();
    public static List<GameObject> items;
    public List<int> m_itemAmmount = new List<int>();
    public static List<int> itemAmmount;
    public int m_currentItem = 0;

    private Rigidbody m_rb;
    private GameObject m_cam;
    private float m_camRot = 0f;
    private Animator m_anim;

    private Vector2 m_movement;
    private Vector2 m_rotation;
    private bool m_jump;

    private bool m_onGround;
    private float m_jumpTime;

    private float m_cameraRestRotation;

    [HideInInspector] public bool m_carying;
    [HideInInspector] public InteractionController m_caryingIC;

    private InteractionController m_lastHighlighted;

    private static PlayerController instance;

    private void Awake() {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    private void Start() {
        if (items != null && items.Count > 0 && itemAmmount != null && itemAmmount.Count > 0) {
            m_items = items.ToArray().ToList();
            m_itemAmmount = itemAmmount.ToArray().ToList();
        }

        m_rb = GetComponent<Rigidbody>();
        m_anim = GetComponentInChildren<Animator>();
        //m_rb.centerOfMass = m_centerOfMass.transform.position;

        m_cam = GameObject.FindGameObjectWithTag("MainCamera");
        m_cam.transform.parent = transform;
        m_cam.transform.position = m_cameraPostion.transform.position;
        m_cam.transform.rotation = m_cameraPostion.transform.rotation;
        m_cameraRestRotation = m_cam.transform.rotation.eulerAngles.x;
    }

    private void Update() {
        if (!m_carying && !GameManager.manager.m_timerRunning)
            HighlightInteractables();
        else if (GameManager.manager.m_timerRunning && m_lastHighlighted != null) {
            m_lastHighlighted.Highlight(true);
            m_lastHighlighted = null;
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        SelectActiveItem();
    }

    private void FixedUpdate() {
        if (m_rb != null) {
            Vector3 yFall = (m_rb.velocity.y * transform.up);

            m_rb.velocity = (m_movement.y * transform.forward * m_speed * Time.deltaTime) + (m_movement.x * transform.right * m_speed * Time.deltaTime) + yFall;
            transform.Rotate(m_rotation.x * transform.up * m_rotSpeed * Time.deltaTime);

            m_camRot += m_rotation.y * -1f * m_rotSpeed * Time.deltaTime;
            m_camRot = Mathf.Clamp(m_camRot, m_cameraRestRotation - m_cameraMaxRot, m_cameraRestRotation);
            Vector3 newRot = m_cam.transform.rotation.eulerAngles;
            newRot.x = m_camRot;
            m_anim.SetFloat("Speed", Mathf.Clamp(m_rb.velocity.magnitude, 0f, 1f));
            m_anim.SetFloat("Speed2", Mathf.Clamp(m_rb.velocity.magnitude, 1f, 5f));

            m_cam.transform.rotation = Quaternion.Euler(newRot);
            
        }
    }

    private void OnCollisionStay(Collision collision) {
        m_onGround = true;
    }

    private void OnCollisionExit(Collision collision) {
        m_onGround = false;
    }

    private void OnMove(InputValue value) {
        Vector2 input = value.Get<Vector2>();
        m_movement = input;
    }

    private void OnLook(InputValue value) {
        Vector2 input = value.Get<Vector2>();
        m_rotation = input;
    }

    private void OnJump(InputValue value) {
        if (m_onGround)
            m_rb.AddForce(new Vector3(0f, m_jumpHeigth, 0f));
    }

    private void OnQuit() {
        Application.Quit();
    }

    private bool OnInteract() {
        if (!GameManager.manager.m_timerRunning) {
            InteractionController ic = CheckForInteractables();
            if (ic != null && !m_carying) {
                if (m_items.Any(x => x.CompareTag(ic.tag))) {
                    m_itemAmmount[m_items.IndexOf(m_items.First(x => x.CompareTag(ic.tag)))]++;
                } else {
                    m_items.Add(ic.gameObject);
                    m_itemAmmount.Add(1);
                }

                Pickup(ic);
                // Flag as deleted so the path will be recalculated accordingly
                if (MovableController.activeMovables.ContainsValue(ic.gameObject)) {
                    FFF.fff_deleted.Add(ic.transform.position);
                }
                return true;

            } else if (m_carying && m_caryingIC != null) {
                Drop();
                return false;
            }
            return false; 
        }
        return false;
    }

    private void NextItem() {
        if (m_currentItem < m_items.Count) {
            m_currentItem++;
            if (m_itemAmmount[m_currentItem - 1] <= 0) {
                NextItem();
            }
        } else if (m_currentItem >= m_items.Count) {
            m_currentItem = 0;
            if (m_caryingIC != null) {
                Destroy(m_caryingIC.gameObject);
                m_carying = false;
                m_caryingIC = null;
            }
        }
    }

    private void LastItem() {
        if (m_currentItem > 1) {
            m_currentItem--;
            if (m_itemAmmount[m_currentItem - 1] <= 0) {
                LastItem();
            }
        } else if (m_currentItem == 0) {
            m_currentItem = m_items.Count;
            if (m_itemAmmount[m_currentItem - 1] <= 0) {
                LastItem();
            }
        } else if (m_currentItem == 1) {
            m_currentItem = 0;
            if (m_caryingIC != null) {
                Destroy(m_caryingIC.gameObject);
                m_carying = false;
                m_caryingIC = null;
            }
        }
    }

    private void OnChangeItem(InputValue value) {
        float input = value.Get<float>();

        if (!GameManager.manager.m_timerRunning) {
            if (input > 0f) {
                NextItem();
            } else if (input < 0f) {
                LastItem();
            } 
        }
    }

    private void OnStart() {
        if (!GameManager.manager.m_timerRunning && !GameManager.manager.m_goalReached) {
            GameManager.manager.m_startTime = Time.time;
            GameManager.manager.m_timerRunning = true;
            Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PLAYER"), LayerMask.NameToLayer("CAR"), true);

            GameObject car = GameObject.FindGameObjectWithTag("Car");
            if (car != null) {
                CarController cc = car.GetComponent<CarController>();
                if (cc != null) {
                    cc.Activate();
                }
            }


            FFF.StartAllSimmulations();

            if (m_carying) {
                m_currentItem = 0;
                if (m_caryingIC != null) {
                    Destroy(m_caryingIC.gameObject);
                    m_carying = false;
                    m_caryingIC = null;
                }
            }
        } else {
            GameManager.manager.Reload();
        }
    }

    private void OnDelete() {
        if (m_carying) {
            m_currentItem = 0;
            if (m_caryingIC != null) {
                Destroy(m_caryingIC.gameObject);
                m_carying = false;
                m_caryingIC = null;
            }
        } else if (!m_carying && m_currentItem == 0) {
            if (OnInteract()) {
                m_currentItem = 0;
                if (m_caryingIC != null) {
                    Destroy(m_caryingIC.gameObject);
                    m_carying = false;
                    m_caryingIC = null;
                }
            }
        }
    }

    public void Reload() {
        items = m_items.ToArray().ToList();
        itemAmmount = m_itemAmmount.ToArray().ToList();

        if (m_carying || m_currentItem != 0)
            OnDelete();
    }

    private void Drop() {
        m_caryingIC.transform.parent = null;
        SceneManager.MoveGameObjectToScene(m_caryingIC.gameObject, SceneManager.GetActiveScene());
        m_caryingIC.Ghost(true);
        m_caryingIC.m_coll.enabled = true;

        if (m_currentItem == 0 && MovableController.movables.ContainsKey(m_caryingIC.m_mc.m_uuid)) {
            Movable mc = MovableController.movables[m_caryingIC.m_mc.m_uuid];
            mc.inactive = false;

            MovableController.movables[m_caryingIC.m_mc.m_uuid] = mc;
        }  else {
            if (!MovableController.spawnedMovables.ContainsKey(m_caryingIC.m_mc.m_uuid)) {
                MovableController.spawnedMovables.Add(m_caryingIC.m_mc.m_uuid, m_items.First(x => x.CompareTag(m_caryingIC.tag)));

                Debug.Log("Spawned new Item");
            }
            m_caryingIC.m_mc.SetLock(false);
        }

        if (m_caryingIC.CompareTag("Conveyor") || m_caryingIC.CompareTag("Launch")) {
            ConveyorBeltController cbc = m_caryingIC.GetComponent<ConveyorBeltController>();
            if (cbc != null) {
                cbc.enabled = true;
            }
        }

        if (m_caryingIC.m_rb != null)
            m_caryingIC.m_rb.isKinematic = false;

        if (m_items.Any(x => x.CompareTag(m_caryingIC.tag))) {
            m_itemAmmount[m_items.IndexOf(m_items.First(x => x.CompareTag(m_caryingIC.tag)))]--;
            if (m_itemAmmount[m_items.IndexOf(m_items.First(x => x.CompareTag(m_caryingIC.tag)))] <= 0f)
                m_currentItem = 0;
        }

        m_caryingIC.Highlight();
        m_lastHighlighted = m_caryingIC;

        m_carying = false;
        m_caryingIC = null;
    }

    private void Pickup(InteractionController ic) {
        ic.m_coll.enabled = false;
        ic.Ghost();

        if (ic.CompareTag("Conveyor") || ic.CompareTag("Launch")) {
            ConveyorBeltController cbc = ic.GetComponent<ConveyorBeltController>();
            if (cbc != null) {
                cbc.enabled = false;
            }
        } else {
            if (ic.m_rb != null)
                ic.m_rb.isKinematic = true;
        }

        
        ic.transform.position = transform.position + transform.forward * 3f;
        ic.transform.position = new Vector3(ic.transform.position.x, ic.transform.position.y + ic.m_caryingHeight, ic.transform.position.z);
        ic.transform.rotation = Quaternion.LookRotation(transform.forward, transform.up);
        ic.transform.parent = transform;

        m_carying = true;
        m_caryingIC = ic;


        if (MovableController.movables.ContainsKey(m_caryingIC.m_mc.m_uuid)) {
            Movable mc = MovableController.movables[m_caryingIC.m_mc.m_uuid];
            mc.inactive = true;
            MovableController.movables[m_caryingIC.m_mc.m_uuid] = mc;
        }
    }

    private void SelectActiveItem() {
        if (m_currentItem != 0 && !m_carying) {
            if (m_itemAmmount[m_currentItem - 1] > 0) {
                m_carying = true;
                StartCoroutine(SwitchActiveItem());
            }
        } else if (m_currentItem != 0 && m_carying && m_caryingIC != null) {
            if (m_itemAmmount[m_items.IndexOf(m_items.First(x => x.CompareTag(m_caryingIC.tag)))] <= 0) {
                Destroy(m_caryingIC.gameObject);
                m_carying = false;
                m_caryingIC = null;
                m_currentItem = 0;
            } else if (!m_items[m_currentItem - 1].CompareTag(m_caryingIC.tag)) {
                Destroy(m_caryingIC.gameObject);
                m_carying = false;
                m_caryingIC = null;
            }
        }
    }

    IEnumerator SwitchActiveItem() {
        if (m_lastHighlighted != null)
            m_lastHighlighted.Highlight(true);
        m_lastHighlighted = null;

        GameObject go = Instantiate(m_items[m_currentItem - 1], new Vector3(500f, 0f, 500f), Quaternion.identity);
        yield return new WaitForSeconds(0.05f);
        InteractionController ic = go.GetComponent<InteractionController>();

        while (!ic.m_ready)
            yield return null;

        Pickup(ic);
    }

    private void HighlightInteractables() {
        InteractionController ic = CheckForInteractables();
        if (ic != m_lastHighlighted && ic != null && ic != m_carying) {
            if (m_lastHighlighted != null)
                m_lastHighlighted.Highlight(true);
            m_lastHighlighted = ic;
            ic.Highlight();
        } else if (ic == null && m_lastHighlighted != null) {
            m_lastHighlighted.Highlight(true);
            m_lastHighlighted = null;
        }
    }

    private InteractionController CheckForInteractables() {
        Collider[] inRange = Physics.OverlapBox(transform.position + transform.forward * 2f, new Vector3(1.5f, 1.5f, 2f));

        float shortest = Mathf.Infinity;
        InteractionController interactable = null;
        foreach(Collider c in inRange) {
            if (c != null && c.gameObject != null) {
                float distance = (c.gameObject.transform.position - transform.position).magnitude;
                if (distance < shortest) {
                    InteractionController ic = c.GetComponentInParent<InteractionController>();
                    if (ic != null && ic != this) {
                        shortest = distance;
                        interactable = ic;
                    }
                }
            }
        }

        return interactable;
    }
}
