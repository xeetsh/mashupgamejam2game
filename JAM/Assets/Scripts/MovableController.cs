﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Movable {
    public string uuid;
    public Vector3 position;
    public Quaternion rotation;
    public bool inactive;
}

public class MovableController : MonoBehaviour {
    public static Dictionary<string, Movable> movables = new Dictionary<string, Movable>();
    public static bool locked = true;

    public string m_uuid = "";
    public static Dictionary<string, GameObject> activeMovables = new Dictionary<string, GameObject>();
    public static Dictionary<string, GameObject> spawnedMovables = new Dictionary<string, GameObject>();
    private bool m_singleLockZ = true;

    public static void Reload() {
        activeMovables = new Dictionary<string, GameObject>();
    }

    public void SetLock(bool b) {
        m_singleLockZ = b;
    }

    public static void Apply() {
        // Map items
        foreach(KeyValuePair<string, GameObject> kvp in activeMovables) {
            if (movables.ContainsKey(kvp.Key)) {
                kvp.Value.transform.position = movables[kvp.Key].position;
                kvp.Value.transform.rotation = movables[kvp.Key].rotation;
                kvp.Value.GetComponent<MovableController>().m_singleLockZ = false;

                if (movables[kvp.Key].inactive)
                    Destroy(kvp.Value);
            } else {
                kvp.Value.GetComponent<MovableController>().m_singleLockZ = false;
            }
        }

        // Spawned items
        foreach (KeyValuePair<string, GameObject> kvp in spawnedMovables) {
            if (movables.ContainsKey(kvp.Key) && !movables[kvp.Key].inactive) {
                GameObject go = Instantiate(kvp.Value, movables[kvp.Key].position, movables[kvp.Key].rotation);
                go.GetComponent<MovableController>().m_singleLockZ = false;
                go.GetComponent<MovableController>().m_uuid = kvp.Key;
            }
        }

        locked = false;
    }

    private void Update() {
        if (m_uuid == "") {
            m_uuid = System.Guid.NewGuid().ToString();
        }

        if (!locked && Application.isPlaying && !m_singleLockZ && !GameManager.manager.m_updateLock) {
            if (m_uuid != "" && !movables.ContainsKey(m_uuid)) {
                Movable m = new Movable();
                m.uuid = m_uuid;
                m.position = transform.position;
                m.rotation = transform.rotation;
                movables.Add(m_uuid, m);
            } else if (m_uuid != "" && movables.ContainsKey(m_uuid)) {
                Movable m = movables[m_uuid];
                m.position = transform.position;
                m.rotation = transform.rotation;
                movables[m_uuid] = m;
            } 
        }
    }

    private void Awake() {
        if (Application.isPlaying) {
            if (m_uuid == "") {
                m_uuid = System.Guid.NewGuid().ToString();
            }

            if (!activeMovables.ContainsKey(m_uuid))
                activeMovables.Add(m_uuid, gameObject); 
        }
    }
}
