﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public struct RecordingDate {
    public string uuid;

    public float timestamp;
    public int iteration;

    public Vector3 position;
    public Quaternion rotation;

    public Vector3 velocity;
    public Vector3 angularVelocity;
}

// Fuck Fysics Fix
public class FFF : MonoBehaviour {
    public static Dictionary<string, List<RecordingDate>> recordedPositions = new Dictionary<string, List<RecordingDate>>();
    public static List<FFF> fff = new List<FFF>();
    public static List<Vector3> fff_deleted = new List<Vector3>();
    public static Dictionary<string, bool> fff_movedLastRound = new Dictionary<string, bool>();
    public bool m_movable;
    private float m_nextRecord = 0f;

    private int m_highestReplayIteration = 0;


    private static float m_recordingSteps = 0.01f;
    private static float m_movementTreshold = 2f;
    private static float m_proximityTreshold = 5f;
    private static float m_simmulateMovablesNearCar = 15f;
    private static float m_simmulationTimeTreshold = 0.5f;

    private Vector3 m_lastPos;
    public bool m_movedThisRound = false;

    private Rigidbody m_rb;
    private MovableController m_mc;
    private Collider[] m_coll;

    private bool m_replay = false;
    private float m_simmulationStopTime;
    private Vector3 m_simmulationStopTimeVel;
    private Vector3 m_simmulationStopTimeAngVel;


    private string m_uuid;

    private void Start() {
        fff.Add(this);

        m_rb = GetComponent<Rigidbody>();
        m_mc = GetComponent<MovableController>();
        m_coll = GetComponentsInChildren<Collider>();

        string uuid = "car";
        if (m_mc != null)
            uuid = m_mc.m_uuid;

        m_uuid = uuid;

        m_lastPos = transform.position;
    }

    private void FixedUpdate() {
        if (!m_replay)
            RecordPath();

        RecordMovement();

        SetReplayPosition();
        
    }

    public static void StartAllSimmulations() {
        if (!recordedPositions.ContainsKey("car"))
            return;

        foreach(FFF f in fff.Where(x => fff_movedLastRound.ContainsKey(x.m_uuid) && fff_movedLastRound[x.m_uuid] && x.m_movable)) {

            f.Replay();

            // Check proximity to placed objects
        }


        //List<FFF> qualified = new List<FFF>();
        //Debug.Log("Starting simmulation for " + fff_movedLastRound.Count(x => x.Value) + " movables!");
        //foreach (KeyValuePair<string, bool> kvp in fff_movedLastRound.Where(x => x.Value)) {
        //    foreach (RecordingDate rd in recordedPositions["car"]) {
        //        FFF o = fff.First(x => x.m_uuid == kvp.Key);
        //        if ((o.transform.position - rd.position).magnitude < m_simmulateMovablesNearCar) {
        //            o.Replay();
        //            break;
        //        }
        //    }
        //}

        //foreach (FFF f in fff) {
        //    if (f != null && fff_movedLastRound.ContainsKey(f.m_uuid) && !f.CompareTag("Car") && !f.m_replay) {
        //        if (fff_movedLastRound[f.m_uuid] && !f.CompareTag("Conveyor") && !f.CompareTag("Launch") && !f.CompareTag("OilSpill") && !f.CompareTag("Explosive"))
        //            f.Replay();
        //    }
        //}
        fff_movedLastRound = new Dictionary<string, bool>();
    }

    Vector3 llllAstpos = Vector3.zero;
    private void SetReplayPosition() {
        // Replay
        if (m_replay && GameManager.manager.m_timerRunning) {
            if (GameManager.manager.m_time > m_simmulationStopTime) {
                m_replay = false;
                foreach (Collider c in m_coll) {
                    c.enabled = true;
                    if (CompareTag("Car"))
                        c.isTrigger = false;
                }
                m_rb.isKinematic = false;
                m_rb.velocity = m_simmulationStopTimeVel;
                m_rb.angularVelocity = m_simmulationStopTimeAngVel;

                return;
            }

            List<RecordingDate> l = recordedPositions[m_uuid];

            Quaternion newRot = Quaternion.identity;
            Vector3 newPos = Vector3.zero;
            float bestTime = Mathf.Infinity;
            RecordingDate rdd = new RecordingDate();
            int newIteration = 0;

            foreach (RecordingDate rd in l) {
                if (Mathf.Abs(rd.timestamp - GameManager.manager.m_time) < bestTime && rd.iteration >= m_highestReplayIteration) {
                    newRot = rd.rotation;
                    newPos = rd.position;
                    rdd = rd;
                    bestTime = Mathf.Abs(rd.timestamp - GameManager.manager.m_time);
                    newIteration = rd.iteration;
                }
            }

            if (bestTime <= m_simmulationTimeTreshold) {
                m_highestReplayIteration = newIteration;
                transform.position = newPos;
                transform.rotation = newRot;
            }

            //if (name == "Wheel (2)") {
            //    if ((newPos - llllAstpos).magnitude > 3f) {
            //        Debug.Log(rdd.iteration);
            //    }
            //    llllAstpos = newPos;
            //    GameObject t = Instantiate(GameManager.manager.m_debugMarker, newPos, Quaternion.identity);
            //    t.transform.localScale = new Vector3(1f, 1f, 1f);
            //    Destroy(t, 0.5f);
            //}
        }
    }

    public void DrawDebugSimmulation(RecordingDate[] markers) {
        int count = 0;
        foreach (RecordingDate v in markers) {
            count++;
            GameObject go = Instantiate(GameManager.manager.m_debugMarker, v.position, Quaternion.identity);
            MaterialPropertyBlock mpb = new MaterialPropertyBlock();
            mpb.SetColor("_Color", Color.HSVToRGB(count * (1f / (255f * 1f)), 1f, 1f));
            go.GetComponent<Renderer>().SetPropertyBlock(mpb);
        }
    }

    public bool Replay() {
        if (recordedPositions.ContainsKey(m_uuid) && SetupPath()) {
            //Debug.Log("Started replay for " + name);

            List<RecordingDate> l = recordedPositions[m_uuid];


            if (l.Count > 0) {
                // Find endpoint
                float highest = 0f;
                Vector3 vel = Vector3.zero;
                Vector3 ang = Vector3.zero;
                foreach (RecordingDate rd in l) {
                    if (rd.timestamp > highest) {
                        highest = rd.timestamp;
                        vel = rd.velocity;
                        ang = rd.angularVelocity;
                    }
                }
                m_simmulationStopTime = highest;
                m_simmulationStopTimeAngVel = ang;
                m_simmulationStopTimeVel = vel;

                m_rb.isKinematic = true;
                foreach (Collider c in m_coll) {
                    if (!CompareTag("Car"))
                        c.enabled = false;
                }

                //DrawDebugSimmulation(recordedPositions[m_uuid].ToArray());

                m_replay = true;
            }
        
            return true;
        
        } else {
            return false;
        }
    }

    private bool SetupPath() {
        foreach(FFF f in fff) {
            if (f == null)
                continue;

            //Dictionary<int, List<RecordingDate>> ll = new Dictionary<int, List<RecordingDate>>();
            //foreach(RecordingDate rd in recordedPositions[m_uuid]) {
            //    if (ll.ContainsKey(rd.iteration)) {
            //        ll[rd.iteration].Add(rd);
            //    } else {
            //        ll.Add(rd.iteration, new List<RecordingDate>() { rd });
            //    }
            //}

            if (f.m_movedThisRound) {

                List<RecordingDate> newRD = new List<RecordingDate>();

                foreach (RecordingDate rd in recordedPositions[m_uuid]) {
                    if ((f.transform.position - rd.position).magnitude > m_proximityTreshold) {
                        newRD.Add(rd);
                    } else {
                        break;
                    }
                }
                recordedPositions[m_uuid] = newRD;
            }
        }

        // Deleted
        foreach (Vector3 f in fff_deleted) {
            if (f == null)
                continue;

            List<RecordingDate> newRD = new List<RecordingDate>();
            foreach (RecordingDate rd in recordedPositions[m_uuid]) {
                if ((f - rd.position).magnitude > m_proximityTreshold) {
                    newRD.Add(rd);
                } else {
                    break;
                }
            }
            recordedPositions[m_uuid] = newRD;
        }

        // POSSIBLE FIX
        //if (!gameObject.CompareTag("Car")) {
        //    List<RecordingDate> preList = recordedPositions[m_uuid].OrderBy(x => x.timestamp).ToList();
        //    List<RecordingDate> finalList = new List<RecordingDate>();
        //    foreach (RecordingDate date in preList) {
        //        RecordingDate oldest = new RecordingDate();
        //        oldest.iteration = -1;
        //        foreach(RecordingDate duplicate in preList.Where(x => Mathf.Abs(date.timestamp - x.timestamp) < 0.05)) {
        //            if (duplicate.iteration > oldest.iteration) {
        //                oldest = duplicate;
        //            }
        //        } 
        //        finalList.Add(oldest);
        //    }

        //    recordedPositions[m_uuid] = finalList.OrderBy(x => x.timestamp).ToList();

        //    int iteration = 0;
        //    List<RecordingDate> finalList2 = new List<RecordingDate>();
        //    foreach (RecordingDate ddd in recordedPositions[m_uuid]) {
        //        if (ddd.iteration <= iteration) {
        //            finalList2.Add(ddd);
        //            iteration = ddd.iteration;
        //        }
        //    }
        //    recordedPositions[m_uuid] = finalList2;
        //}

        if (recordedPositions[m_uuid] == null || recordedPositions[m_uuid].Count == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void RecordMovement() {
        if (GameManager.manager != null && !GameManager.manager.m_timerRunning && !GameManager.manager.m_goalReached) {
            // If moved significantly save move iteration
            if ((transform.position - m_lastPos).magnitude > m_movementTreshold) {
                m_movedThisRound = true;

                m_lastPos = transform.position;
            } 
        } else if (GameManager.manager != null && GameManager.manager.m_timerRunning) {
            if ((transform.position - m_lastPos).magnitude > m_movementTreshold) {
                if (fff_movedLastRound.ContainsKey(m_uuid)) {
                    fff_movedLastRound[m_uuid] = true;
                } else {
                    fff_movedLastRound.Add(m_uuid, true);
                }
            }
        }
    }

    private void RecordPath() {
        if (GameManager.manager != null && GameManager.manager.m_timerRunning) {
            if (GameManager.manager.m_time > m_nextRecord) {
                // Record Path
                RecordingDate rd = new RecordingDate();
                rd.uuid = m_uuid;
                rd.timestamp = GameManager.manager.m_time;
                rd.iteration = WasteManager.count;
                rd.position = transform.position;
                rd.rotation = transform.rotation;
                rd.velocity = m_rb.velocity;
                rd.angularVelocity = m_rb.angularVelocity;

                if (!recordedPositions.ContainsKey(m_uuid)) {
                    recordedPositions.Add(m_uuid, new List<RecordingDate>());
                }

                recordedPositions[m_uuid].Add(rd);

                m_nextRecord = GameManager.manager.m_time + m_recordingSteps;
            }
        }
    }
}
