﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WasteManager : MonoBehaviour {
    private static WasteManager instance = null;
    public static int count = 0;

    public UIController m_ui;
    private void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);

            Debug.Log("First Start");
            MovableController.spawnedMovables = new Dictionary<string, GameObject>();
            MovableController.movables = new Dictionary<string, Movable>();
            m_ui.FirstStart();
            count++;
        } else if (instance != this) {
            count++;
            FFF.fff = new List<FFF>();
            FFF.fff_deleted = new List<Vector3>();
            Destroy(gameObject);
        }
    }
}
