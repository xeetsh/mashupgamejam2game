﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalController : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Car")) {
            if (GameManager.manager != null && GameManager.manager.m_timerRunning && !GameManager.manager.m_goalReached)
                GameManager.manager.m_goalReached = true;
                GameManager.manager.m_timerRunning = false;
        }
    }
}
