﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBeltController : MonoBehaviour {
    public float m_speed;

    private Rigidbody m_rb;

    private void Start() {
        m_rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate() {
        if (GameManager.manager.m_timerRunning) {
            Vector3 pos = m_rb.position;
            m_rb.position += transform.forward * -1f * m_speed * Time.fixedDeltaTime;
            m_rb.MovePosition(pos);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        m_rb.isKinematic = true;
    }
}
