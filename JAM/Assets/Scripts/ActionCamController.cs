﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCamController : MonoBehaviour {
    public GameObject m_car;
    public float m_lerpspeed;

    private Vector3 m_offset;

    private void Start() {
        m_offset = transform.position - m_car.transform.position;
    }

    private void Update() {
        transform.position = Vector3.Lerp(transform.position, m_car.transform.position + m_offset, Time.deltaTime * m_lerpspeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation((m_car.transform.position - transform.position), transform.up), Time.deltaTime * m_lerpspeed);
    }
}
