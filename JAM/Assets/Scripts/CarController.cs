﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CarController : MonoBehaviour {
    public float m_maxSpeed;
    public GameObject m_trajectoryMarker;
    public GameObject m_debugMarker;
    public Material m_ghostMaterial;
    public Light[] m_lights;

    private Material[] m_defaultMaterial;

    private Rigidbody m_rb;
    private Collider[] m_coll;
    private Renderer[] m_renderer;

    private GameObject m_lastMarker;

    public List<GameObject> m_trajectoryMarkers = new List<GameObject>();
    public static List<Vector3> trajectoryMarkerPositions = new List<Vector3>();

    private void Start() {
        m_rb = GetComponent<Rigidbody>();

        m_rb.isKinematic = true;

        m_renderer = GetComponentsInChildren<Renderer>();
        m_coll = GetComponents<Collider>();
        m_defaultMaterial = new Material[m_renderer.Length];

        for (int i = 0; i < m_renderer.Length; i++) {
            m_defaultMaterial[i] = m_renderer[i].material;
            m_renderer[i].material = m_ghostMaterial;
        }

        
        foreach (Collider c in m_coll) {
            c.enabled = false;
        }

        DrawOldPath();
    }

    private void Update() {
        if (m_lastMarker == null || (m_lastMarker.transform.position - transform.position).magnitude > 0.5f) {
            m_lastMarker = Instantiate(m_trajectoryMarker, transform.position, Quaternion.identity);
            trajectoryMarkerPositions.Add(transform.position);
            m_trajectoryMarkers.Add(m_lastMarker);
        }
    }

    private void FixedUpdate() {
        if (m_rb.velocity.magnitude > m_maxSpeed) {
            float limit = m_rb.velocity.magnitude / m_maxSpeed;
            m_rb.velocity = m_rb.velocity / limit;
        }
    }

    public void DrawOldPath() {
        foreach (Vector3 v in trajectoryMarkerPositions) {
            m_lastMarker = Instantiate(m_trajectoryMarker, v, Quaternion.identity);
            m_trajectoryMarkers.Add(m_lastMarker);
        }
    }

    public void DrawSimmulation(Vector3[] vv) {
        foreach (Vector3 v in vv) {
            m_lastMarker = Instantiate(m_debugMarker, v, Quaternion.identity);
        }
    }

    public void Activate() {
        // Activate lights
        foreach(Light l in m_lights) {
            l.enabled = true;
        }

        // Delete old path
        foreach (GameObject go in m_trajectoryMarkers) {
            Destroy(go);
        }
        m_trajectoryMarkers = new List<GameObject>();
        trajectoryMarkerPositions = new List<Vector3>();

        for (int i = 0; i < m_renderer.Length; i++) {
            m_renderer[i].material = m_defaultMaterial[i];
        }

        if (!FFF.recordedPositions.ContainsKey("car")) {
            m_rb.isKinematic = false;
            foreach (Collider c in m_coll) {
                c.enabled = true;
            }
        } else {
            // Replay
            m_rb.isKinematic = false;
            foreach (Collider c in m_coll) {
                c.enabled = true;
                c.isTrigger = true;
            }

            if (!GetComponent<FFF>().Replay()) {
                m_rb.isKinematic = false;
                foreach (Collider c in m_coll) {
                    c.enabled = true;
                    c.isTrigger = false;
                }
            }
        }
    }
}
