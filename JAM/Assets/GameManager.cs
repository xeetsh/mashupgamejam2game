﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public static GameManager manager;
    public static float best = Mathf.Infinity;
    public GameObject m_debugMarker;

    [HideInInspector] public bool m_timerRunning;
    [HideInInspector] public bool m_goalReached;
    [HideInInspector] public float m_startTime;
    [HideInInspector] public float m_time;
    [HideInInspector] public bool m_updateLock = false;

    private void Awake() {
        MovableController.locked = true;

        if (manager == null) {
            manager = this;
        } else { 
            Destroy(this);
        }
    }

    private void Start() {
        MovableController.Apply();

        QualitySettings.vSyncCount = 1;
    }

    private void Update() {
        if (m_timerRunning)
            m_updateLock = true;

        if (m_timerRunning && m_startTime != 0f) {
            m_time = Time.time - m_startTime;
        }
    }

    public void Reload() {
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PLAYER"), LayerMask.NameToLayer("CAR"), false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().Reload();
        MovableController.Reload();

        if (!m_timerRunning && m_goalReached) {
            if (m_time < best)
                best = m_time;
        }


        m_timerRunning = false;
        m_goalReached = false;
        m_time = 0f;
        m_startTime = 0f;
    }
}
